import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  constructor(private classify:ClassifyService,private router:Router) { }

  text:string;

  
  onSubmit(){
    this.classify.doc = this.text;
    this.router.navigate(['/saveclassify']);
  }

  ngOnInit() {
  
  }

}
