import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './interfaces/post';
import { Comment } from './interfaces/comment';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  apipost = "https://jsonplaceholder.typicode.com/posts"
  apicomment = "http://jsonplaceholder.typicode.com/comments"
  
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  postCollection:AngularFirestoreCollection;

  
  getpostofuser(userId):Observable<any[]>{
    this.postCollection = this.db.collection(`users/${userId}/posts`);
        console.log('Books collection created');
        return this.postCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
            return { ...data };
          }))
        ); 
  }

  addpost(userId:string,title:string, body:string){
    const post = {title:title, body:body,like:0};
    this.userCollection.doc(userId).collection('posts').add(post);
  }

  
  getonepost(userId, id:string):Observable<any>{
    return this.db.doc(`users/${userId}/posts/${id}`).get();
  }

  updatepostlike(userId:string, id:string,like:number){
    this.db.doc(`users/${userId}/posts/${id}`).update(
       {
         like:like
       }
     )
   }

  getPost():Observable<any>{
    return this.http.get<Post[]>(this.apipost);
  }

  getcomment():Observable<any>{
    return this.http.get<Comment[]>(this.apicomment);
  }


  constructor(private http: HttpClient,private db:AngularFirestore) { }
}
