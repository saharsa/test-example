import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  email:string;
  password:string;
  hide = true;
  public errorMessage:string;

  constructor(public auth:AuthService,private router:Router) { 
    this.auth.getLoginErrors().subscribe(error => {
      this.errorMessage = error;
    });
    auth.err = "";
  }

  onSubmit(){
    this.auth.signup(this.email,this.password);
    
  }

  ngOnInit() {
  }

}
