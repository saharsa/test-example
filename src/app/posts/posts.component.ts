import { Component, OnInit } from '@angular/core';
import { Post } from '../interfaces/post';
import { PostService } from '../post.service';
import { AuthService } from '../auth.service';
import { Comment } from '../interfaces/comment';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  Posts$:Observable<Post[]>;
  comments$:Observable<Comment[]>;
  userId:string;
  text:string;
  postid:number;
  
  constructor(private postsrv: PostService,private auth:AuthService) { }

  add(title:string,body:string,id:number){
    this.postsrv.addpost(this.userId,title,body)
    this.postid = id;
    this.text = "Saved for later viewing"
  }

  ngOnInit() {
    this.postsrv.getPost()
    .subscribe(data =>this.Posts$ = data );
    this.postsrv.getcomment()
    .subscribe(data =>this.comments$ = data );

    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
       }
    )
  }

}
